let questionList = [];
let question;
let buttonLabels = ['aButton', 'bButton', 'cButton', 'dButton'];
let questionNumber = 0;
let userAnswers = [];

const howManyQuestions = 15;


function fillQuestion() {
    $('#question')[0].textContent = question.questionText;
    let answers = question.answers;
    for (let i = 0; i < answers.length; i++) {
        $('#' + buttonLabels[i])[0].value = answers[i];
    }
}

function startQuiz() {
    questionList = generateQuestionList(howManyQuestions);
    initiateProgressBar(howManyQuestions);
    loadQuestion();
}

function loadQuestion() {
    question = questionList[questionNumber];
    question.randomizeAnswers();
    fillQuestion();
    $('#questionContainer').fadeIn();
    run();
}


function run() {
    startTimer();
}

function timeEnded() {
    doNextStep();
}

function showResults() {
    let containerQuestion = $('.container-question')[0];
    containerQuestion.style.display = 'none';
    let containerAnswer = $('.container-anwer')[0];
    containerAnswer.style.display = '';

    let goodAnswers = 0;
    for (let i = 0; i < userAnswers.length; i++) {
        if (userAnswers[i] === true) {
            goodAnswers++;
        }
    }

    $('#result')[0].textContent = "You did good on " + goodAnswers.toString() + " out of " + userAnswers.length.toString() + " questions.";
}

function doNextStep(value) {
    saveAnswer(value);
    $('#questionContainer').fadeOut(300,function () {
        questionNumber++;
        stopTimer();
        updateProgress(questionNumber)
        if (questionNumber < questionList.length) {
            loadQuestion();
        } else {
            showResults();
        }
    });

}

function saveAnswer(value) {

    if (value) {
        if (value === question.correctAnswer.toString()) {
            userAnswers[questionNumber] = true;
            console.log("Good answer");
        } else {
            userAnswers[questionNumber] = false;
            console.log("Bad answer");
        }
    } else {
        userAnswers[questionNumber] = false;
        console.log("Bad answer");
    }

}





