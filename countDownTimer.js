let expires;
let counter;

function startTimer() {
    expires = new Date();
    expires.setSeconds(expires.getSeconds() + 10);
    counter = setInterval(timer, 1);
}

function timer() {
    let timeDiff = expires - new Date();

    if (timeDiff <= 0) {
        stopTimer();
        timeEnded();
        return;
    }

    let seconds = new Date(timeDiff).getSeconds();
    let milliSeconds = (new Date(timeDiff).getMilliseconds() / 10).toFixed(0);

    seconds = seconds < 10 ? "0" + seconds : seconds;
    milliSeconds = milliSeconds < 10 ? "0" + milliSeconds : milliSeconds;

    document.getElementById("timer").innerHTML = seconds + ":" + milliSeconds; // watch for spelling
}

function stopTimer() {
    counter = clearInterval(counter);
}