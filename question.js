function Question(questionText,answer0,answer1,answer2,answer3,correctAnswer) {
    this.questionText = questionText;
    this.answers = [answer0, answer1, answer2, answer3];
    this.correctAnswer = this.answers[correctAnswer];
    this.randomizeAnswers = function () {
        let array = this.answers;
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        this.answers = array;
    };
}

function generateQuestionList(size) {
    let questionList = [];
    for (let i = 0 ; i < size ; i++){
        questionList[i] = generateQuestion(100);
    }
    return questionList;
}

function generateQuestion(max) {
    let firstDigit = getNumber(max);
    let secondDigit = getNumber(max);

    let result = firstDigit + secondDigit;

    let falseResults = [];
    for (let i = 0 ; i < 3 ; i++){
        falseResults[i]=getFalseResult(result,2*max,falseResults);
    }

    return new Question("What is " + firstDigit + " + " + secondDigit + " ?",
        result, falseResults[0], falseResults[1], falseResults[2], 0);
}

function getNumber(max) {
    return Math.ceil(Math.random() * max);
}

function getFalseResult(result,max,otherFalseResults) {
    let falseResult = getNumber(max);
    let isValidFalseResult = true;

    if (falseResult === result){
        isValidFalseResult = false;
    }

    if (otherFalseResults){
        for(let i = 0; i < otherFalseResults.length ; i++){
            if (falseResult===otherFalseResults[i]){
                isValidFalseResult = false;
            }
        }
    }

    if (isValidFalseResult === true){
        return falseResult;
    }else {
        return getFalseResult(result,max,otherFalseResults)
    }
}



