const progressPartID = "progressPartID"
let progress = 0;
let maxProgress = 0;

function initiateProgressBar(questionsAmount) {
    maxProgress = questionsAmount;
    for(let i = 0 ; i < maxProgress ; i++){
        let part = document.createElement('div');
        part.id = progressPartID + i.toString();
        part.className = getProgressStepType(i);
        let label = document.createElement('label');
        label.textContent = (i+1).toString();
        part.appendChild(label);
        let width = 100/questionsAmount;
        $('#progressRow')[0].appendChild(part);
        part.style.width = width.toString() + '%';
    }
}

function getProgressStepType(index) {
    let type;
    if(index<progress){
        type = 'progressStep before';
    }else if (index === progress){
        type = 'progressStep active';
    }else {
        type = 'progressStep future';
    }
    return type;
}



function updateProgress(questionNumber) {
    progress = questionNumber;
    for(let i = 0; i < maxProgress ; i++){
        $('#'+progressPartID+i.toString())[0].className = getProgressStepType(i);
    }
}

